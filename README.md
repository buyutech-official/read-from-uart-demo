### Read From Uart

This project read data from uart and show image. Data is base64 encrypted jpeg compressed images received from the camera. Device sends data by default. You can control the device sending and stopping data with sending start-stop signals. 

### Dependencies
- cmake
- OpenCV

### Build and Run
```shellscript
mkdir build && cd build
cmake ..
make
./readFromUart
```

### Data
##### Sendable Data
`,` : Signal to start the device to send images via uart

`.` : Signal to stop the device to send images via uart
##### Incoming Data
`[` : Signal the beginning of the image

`]` : Signal the ending of the image

`data` : Base64 encoded jpeg image between ‘[‘ and ‘]’