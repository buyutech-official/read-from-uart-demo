//
// Created by BUYUTECH on 24.04.2020.
// This class is for read data from UART and send start-stop signals to UART.
//
// Copyright (C) 2020  BUYUTECH
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#include "UartManager.h"
#include <iostream>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <opencv2/opencv.hpp>

std::string m_base64Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                            "abcdefghijklmnopqrstuvwxyz"
                            "0123456789+/";

UartManager *UartManager::instance = nullptr;

UartManager *UartManager::getInstance() {
    if (instance == nullptr) {
        instance = new UartManager();
    }
    return instance;
}

UartManager::UartManager() {
    fd = initUart();
}

/**
 * Initialize uart
 * @return file descriptor for uart
 */
int UartManager::initUart() {

    std::string portName = "/dev/ttyUSB0"; // Uart port name
    int fd = open(portName.c_str(), O_RDWR | O_NOCTTY | O_SYNC);

    if (fd == -1)                                               /* Error Checking */
        std::cout << "Error! in Opening " << portName << std::endl;
    else
        std::cout << portName << " Opened Successfully " << std::endl;


    /*---------- Setting the Attributes of the serial port using termios structure --------- */

    struct termios SerialPortSettings;          /* Create the structure                          */

    tcgetattr(fd, &SerialPortSettings);         /* Get the current attributes of the Serial port */

    cfsetospeed(&SerialPortSettings, B4000000);        /* Set Write  Speed as 4000000                       */
    cfsetispeed(&SerialPortSettings, B4000000);        /* Set Read Speed as 4000000                       */

    SerialPortSettings.c_cflag &= ~PARENB;          /* Disables the Parity   Enable bit(PARENB),So No Parity   */
    SerialPortSettings.c_cflag &= ~CSTOPB;          /* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
    SerialPortSettings.c_cflag &= ~CSIZE;           /* Clears the mask for setting the data size             */
    SerialPortSettings.c_cflag |= CS8;             /* Set the data bits = 8                                 */

    SerialPortSettings.c_cflag &= ~CRTSCTS;         /* No Hardware flow Control                         */
    SerialPortSettings.c_cflag |= CREAD | CLOCAL;   /* Enable receiver,Ignore Modem Control lines       */


    SerialPortSettings.c_iflag &= ~(IXON | IXOFF | IXANY);          /* Disable XON/XOFF flow control both i/p and o/p */
    SerialPortSettings.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);  /* Non Cannonical mode                            */

    SerialPortSettings.c_oflag &= ~OPOST;   /*No Output Processing*/

/* Setting Time outs */
    SerialPortSettings.c_cc[VMIN] = 10; /* Read at least x characters */
    SerialPortSettings.c_cc[VTIME] = 0; /* Wait indefinetly   */

    if ((tcsetattr(fd, TCSANOW, &SerialPortSettings)) != 0) /* Set the attributes to the termios structure*/
        std::cout << std::endl << "ERROR ! in Setting attributes" << std::endl;
    else {
        std::cout << "BaudRate = 4000000" << std::endl;
    }
    return fd;
}

/**
 * Send signal to uart to start or stop sending image
 * @param signal : Start or stop signal
 */
void UartManager::sendSignal(UART_SIGNAL signal) {
    std::string strSignal = "";
    std::cout << "Sending signal..." << std::endl;
    switch (signal) {
        case START_SEND_IMAGE: {
            strSignal = ",";
            break;
        }
        case STOP_SEND_IMAGE: {
            strSignal = ".";
            break;
        }
    }

    strSignal += "\n";  // To flush uart buffer send eos

    int count = write(fd, strSignal.c_str(), strSignal.length()); // Write to uart
    if (count != strSignal.length()) {
        std::cout << "Error while sending signal!" << std::endl;
    }
}

/**
 * Read data from uart and show image.
 * This method have infinite loop. Run this method in thread to do other jobs
 */
void UartManager::listen() {
    char read_buffer;

    //First image can be half. So this loop ignore the first image
    while (true) {
        read(fd, &read_buffer, 1); /*Read the data char by char*/
        if (read_buffer == ']') {
            break;
        }
    }

    std::string base64EncodedImage = "";
    // Read data and show image
    while (true) {
        read(fd, &read_buffer, 1); /*Read the data char by char*/
        if (read_buffer != '[' && read_buffer != ']') { // Incoming data. Push buffer
            base64EncodedImage += read_buffer;
        } else if (read_buffer == '[') {    // Start signal. Empty incoming data buffer
            base64EncodedImage = "";
        } else {    // Stop signal. Decode incoming data and show image

            std::string strImage = base64Decode(base64EncodedImage); // Decode incoming data

/*
            // Save image
            char outfile[20];
            sprintf(outfile, "image.jpg");
            FILE *of = fopen(outfile, "wb");
            if (of == NULL) {
                fprintf(stderr, "File %s could not be created\n", outfile);
            } else {
                if (fwrite(strImage.c_str(), 1, strImage.length(), of) != strImage.length()) {
                    fprintf(stderr, "Could not write to file %s\n", outfile);
                }
                fclose(of);
            }
/**/

            // Show image
            cv::Mat rawData(1, strImage.length(), CV_8SC1, (void *) strImage.c_str());  // Convert data to opencv mat
            cv::Mat decodedMat = imdecode(rawData, CV_LOAD_IMAGE_COLOR);
            if (decodedMat.rows > 0 && decodedMat.cols > 0) {   // If there is image, show
                cv::imshow("fromUart", decodedMat);
                cv::waitKey(10);
            }
/**/
            base64EncodedImage = "";
        }
    }
}

/**
 * is base64 character or not
 * @param c : char
 * @return Is c Base64 char
 */
bool UartManager::isBase64(unsigned char c) {
    return (isalnum(c) || (c == '+') || (c == '/'));
}

/**
 * Decodes base64 encoded string
 * @param encodedString : Base64 encoded string
 * @return decoded string
 */
std::string UartManager::base64Decode(std::string const &encodedString) {
    int length = encodedString.size();
    int i = 0;
    int j = 0;
    int in_ = 0;
    unsigned char char_array_4[4], char_array_3[3];
    std::string ret;

    while (length-- && (encodedString[in_] != '=') && isBase64(encodedString[in_])) {
        char_array_4[i++] = encodedString[in_];
        in_++;
        if (i == 4) {
            for (i = 0; i < 4; i++)
                char_array_4[i] = m_base64Chars.find(char_array_4[i]);

            char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
            char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
            char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

            for (i = 0; (i < 3); i++)
                ret += char_array_3[i];
            i = 0;
        }
    }

    if (i) {
        for (j = 0; j < i; j++)
            char_array_4[j] = m_base64Chars.find(char_array_4[j]);

        char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
        char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);

        for (j = 0; (j < i - 1); j++)
            ret += char_array_3[j];
    }

    return ret;
}
