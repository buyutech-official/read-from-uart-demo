//
// Created by BUYUTECH on 24.04.2020.
// This class is for read data from UART and send start-stop signals to UART.
//
// Copyright (C) 2020  BUYUTECH
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#ifndef SENDIMAGETOUART_UARTMANAGER_H
#define SENDIMAGETOUART_UARTMANAGER_H


#include <string>
#include "UartSignal.h"

class UartManager {
public:
    static UartManager *getInstance();

    void sendSignal(UART_SIGNAL signal);

    void listen();

private:
    static UartManager *instance;
    int fd;

    UartManager();

    int initUart();

    bool isBase64(unsigned char c);

    std::string base64Decode(std::string const &encodedString);
};


#endif //SENDIMAGETOUART_UARTMANAGER_H
