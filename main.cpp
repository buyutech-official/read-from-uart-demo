//
// Created by BUYUTECH on 24.04.2020.
//
// Copyright (C) 2020  BUYUTECH
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//

#include <iostream>
#include <thread>
#include "UartManager.h"

UartManager *um;

/**
 * Simulation for start stop signal
 *
 */
void listenAction() {
    std::string c;
    for (;;) {
        std::cin >> c;
        if (c == "1") {
            um->sendSignal(START_SEND_IMAGE);
        } else if (c == "0") {
            um->sendSignal(STOP_SEND_IMAGE);
        }
        c = "";
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
}

int main(int argc, char *argv[]) {
    um = UartManager::getInstance();

    // Listen keyboard and simulate start - stop signals
    std::thread thread(listenAction);
    thread.detach();

    // Detach thread to read data async from uart
    std::thread thread2([](){
        um->listen();
    });
    thread2.detach();

    // Other jobs can be done here
    while (true) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    return 0;
}
